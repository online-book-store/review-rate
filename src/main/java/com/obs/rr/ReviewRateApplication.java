package com.obs.rr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReviewRateApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReviewRateApplication.class, args);
	}

}
